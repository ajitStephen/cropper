import './App.css'
import Main from './app/Main'
import ErrorBoundary from './ErrorBoundary'
import UiConfigProvider from './ui/UiConfigProvider'

const App = () => (
  <ErrorBoundary>
    <UiConfigProvider>
      <Main />
    </UiConfigProvider>
  </ErrorBoundary>
)

export default App
