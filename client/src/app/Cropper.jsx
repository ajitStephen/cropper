import { Rect, Transformer } from 'react-konva'
import useCropper from '../hooks/useCropper'

const Cropper = ({
  x,
  y,
  width,
  height,
  vWidth,
  vHeight,
  rectRef,
  trRef,
  onChange,
  onRectTap,
  onTransformEnd,
  onDragEnd,
}) => {
  const { boundBoxFunc, dragBoundFunc } = useCropper({
    width,
    height,
    vHeight,
    vWidth,
  })
  return (
    <>
      <Rect
        draggable
        ref={rectRef}
        x={x}
        y={y}
        width={width}
        height={height}
        onTap={onRectTap}
        onClick={onRectTap}
        onTransformEnd={onTransformEnd}
        onDragEnd={onDragEnd}
        dragBoundFunc={dragBoundFunc}
      />
      <Transformer
        keepRatio
        rotateEnabled={false}
        ref={trRef}
        enabledAnchors={['top-left', 'top-right', 'bottom-left', 'bottom-right']}
        boundBoxFunc={boundBoxFunc}
      />
    </>
  )
}

export default Cropper
