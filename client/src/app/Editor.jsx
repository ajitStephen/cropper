import { Image, Layer, Stage } from 'react-konva'
import useEditor from '../hooks/useEditor'
import UiCard from '../ui/UiCard'
import UiError from '../ui/UiError'
import UiLoading from '../ui/UiLoading'
import UiButton from '../ui/UiButton'
import UiHorizontalSpace from '../ui/space/UiHorizontalSpace'
import Cropper from './Cropper'
import UiVerticalSpace from '../ui/space/UiVerticalSpace'

const Editor = ({ width, height, imgData, setImage, setCropped }) => {
  const {
    vWidth,
    vHeight,
    loading,
    error,
    image,
    cropper,
    cropperEventHandlers,
    saveCrop,
    cropping,
  } = useEditor({
    width,
    height,
    imgData,
    setImage,
    setCropped,
  })

  if (!imgData) {
    return ''
  }

  if (loading) {
    return <UiLoading style={{ width }} />
  }

  if (error) {
    return <UiError title={error?.name} message={error?.message} />
  }

  let centerAlignSmallImage = {}
  if (vWidth < width) {
    const distributeWidth = width - vWidth
    const left = distributeWidth / 2
    centerAlignSmallImage = { position: 'absolute', left }
  }

  return (
    <UiCard
      bordered={false}
      style={{ width, ...centerAlignSmallImage, backgroundColor: '#FFF9EF' }}
      bodyStyle={{ padding: 0, width: vWidth }}
    >
      <UiVerticalSpace>
        <Stage listening={!cropping} width={vWidth} height={vHeight}>
          <Layer>
            <Image
              x={0}
              y={0}
              image={image}
              width={vWidth}
              height={vHeight}
              opacity={1}
              // scale={{ x: 1.2, y: 1.2 }}
            />
            <Cropper {...cropper} {...cropperEventHandlers} vWidth={vWidth} vHeight={vHeight} />
          </Layer>
        </Stage>

        <UiHorizontalSpace style={{ justifyContent: 'center', width: vWidth }}>
          <UiButton type="primary" onClick={() => saveCrop()}>
            Crop
          </UiButton>
          <UiButton danger onClick={() => setImage(null)}>
            Cancel
          </UiButton>
        </UiHorizontalSpace>
      </UiVerticalSpace>
    </UiCard>
  )
}

export default Editor
