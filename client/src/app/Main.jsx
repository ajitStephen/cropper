import Uploader from '../components/Uploader'
import Editor from './Editor'
import useMain from '../hooks/useMain'

const Main = () => {
  const { image, setImage, width, height, cropped, setCropped } = useMain()

  if (cropped) {
    return (
      <div style={{ width, height, backgroundColor: '#FFF9EF' }}>
        <img src={cropped} alt="cropped-result" width={width} />
      </div>
    )
  }

  return (
    <div style={{ width, height, backgroundColor: '#FFF9EF' }}>
      <Uploader width={width} height={height} image={image} setImage={setImage} />
      <Editor
        height={height}
        width={width}
        imgData={image}
        setImage={setImage}
        setCropped={setCropped}
      />
    </div>
  )
}

export default Main
