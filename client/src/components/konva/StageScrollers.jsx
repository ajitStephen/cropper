import React from 'react'
import { Rect, Layer } from 'react-konva'
import { stroke } from '../../helpers/konva-helpers'

const PADDING = 5

const StageScrollers = ({
  width,
  height,
  stageRef,
  layerRef,
  verticalScrollBar,
  horizontalScrollBar,
  onHorizontalScroll,
  onVerticalScroll,
}) => (
  <Layer onMouseDown={e => (e.cancelBubble = true)} onMouseUp={e => (e.cancelBubble = true)}>
    <Rect
      draggable
      ref={verticalScrollBar}
      x={stageRef?.current?.width() - PADDING - 10}
      y={PADDING}
      width={10}
      height={100}
      fill={stroke.color}
      opacity={0.8}
      dragBoundFunc={pos => {
        pos.x = stageRef?.current?.width() - PADDING - 10
        pos.y = Math.max(
          Math.min(
            pos.y,
            stageRef?.current?.height() - verticalScrollBar?.current?.height() - PADDING
          ),
          PADDING
        )
        return pos
      }}
      onDragMove={() => {
        const availableHeight =
          stageRef?.current?.height() - PADDING * 2 - verticalScrollBar?.current?.height()
        const delta = (verticalScrollBar?.current?.y() - PADDING) / availableHeight
        onVerticalScroll(-(height - stageRef?.current?.height()) * delta)
      }}
    />
    <Rect
      ref={horizontalScrollBar}
      width={100}
      height={10}
      fill={stroke.color}
      opacity={0.8}
      x={PADDING}
      y={stageRef?.current?.height() - PADDING - 10}
      draggable
      dragBoundFunc={pos => {
        pos.x = Math.max(
          Math.min(
            pos.x,
            stageRef?.current?.width() - horizontalScrollBar?.current?.width() - PADDING
          ),
          PADDING
        )
        pos.y = stageRef?.current?.height() - PADDING - 10

        return pos
      }}
      onDragMove={() => {
        const availableWidth =
          stageRef?.current?.width() - PADDING * 2 - horizontalScrollBar?.current?.width()
        const delta = (horizontalScrollBar?.current?.x() - PADDING) / availableWidth
        onHorizontalScroll(-(width - stageRef?.current?.width()) * delta)
      }}
    />
  </Layer>
)

export default StageScrollers
