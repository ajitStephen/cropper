import useUploader from '../hooks/useUploader'

const Uploader = ({ width, height, image, setImage }) => {
  const { inputRef, onImageClick, getBase64 } = useUploader()

  if (image) {
    return ''
  }

  return (
    <div style={{ cursor: 'pointer', textAlign: 'center' }}>
      <img
        onClick={() => onImageClick()}
        width={width * 0.7}
        // height={height}
        src="./images/click_to_upload.jpeg"
        alt="click upload"
      />
      <input
        id="upload-input"
        multiple={false}
        accept="image/*"
        ref={inputRef}
        type="file"
        name="image"
        onChange={async e => {
          const [file] = inputRef?.current?.files
          if (file) {
            const url = URL.createObjectURL(file)
            const data = await getBase64(url)
            setImage({ name: file?.name, data, url, type: file?.type })
          }
        }}
        style={{ display: 'none' }}
      />
    </div>
  )
}

export default Uploader
