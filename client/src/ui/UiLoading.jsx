import UiSpin from './UiSpin'
import UiAlert from './UiAlert'

const UiLoading = ({ widthDeduction, style = {}, message = 'We are loading your data...' }) => (
  <div style={{ width: window.innerWidth - widthDeduction, ...style }}>
    <UiSpin>
      <UiAlert message={message} />
    </UiSpin>
  </div>
)

export default UiLoading
