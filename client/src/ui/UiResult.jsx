import React from 'react'
import Result from 'antd/lib/result'
import 'antd/lib/result/style/css'

const UiResult = ({ status = '404', title = '404', message = '' }) => (
  <Result status={status} title={title} subTitle={message} />
)

export default UiResult
