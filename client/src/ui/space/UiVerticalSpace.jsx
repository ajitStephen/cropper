import UiSpace from '../UiSpace'

const UiVerticalSpace = ({ children, ...rest }) => {
  return (
    <UiSpace direction="horizontal" {...rest}>
      {children}
    </UiSpace>
  )
}

export default UiVerticalSpace
