import UiSpace from '../UiSpace'

const UiHorizontalSpace = ({ children, ...rest }) => {
  return (
    <UiSpace direction="horizontal" {...rest}>
      {children}
    </UiSpace>
  )
}

export default UiHorizontalSpace
