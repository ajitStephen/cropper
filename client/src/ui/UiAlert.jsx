import React from 'react'
import Alert from 'antd/lib/alert'
import 'antd/lib/alert/style/css'

const UiAlert = ({ message, type, showIcon, ...rest }) => (
  <Alert message={message} type={type} showIcon={showIcon} {...rest} />
)

export default UiAlert
