import React from 'react'
import UiResult from './UiResult'

const UiError = ({ title = '500', message = 'Sorry, something went wrong.' }) => {
  return (
    <div style={{ width: window?.innerWidth, textAlign: 'center' }}>
      <UiResult status="500" title={title} message={message} />
    </div>
  )
}

export default UiError
