import React from 'react'
import Modal from 'antd/lib/modal'
import 'antd/lib/modal/style/css'

const UiModalCore = ({ children, ...props }) => <Modal {...props}>{children}</Modal>

export default UiModalCore
