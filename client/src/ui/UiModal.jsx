import React from 'react'
import UiModalCore from './UiModalCore'
import UiButton from './UiButton'
import UiSpace from './UiSpace'

const UiModal = ({
  callback,
  visible,
  loading,
  title,
  returnText,
  submitText,
  children,
  width,
  footer,
  noFooter,
  submitButtonDisabled,
  style,
  ...rest
}) => {
  const handleOk = () => {
    callback({ cancel: false })
  }

  const handleCancel = () => {
    callback({ cancel: true })
  }

  const renderFooter = () => {
    if (noFooter) {
      return ''
    }

    if (footer) {
      return footer
    }

    return (
      <UiSpace>
        <UiButton
          disabled={submitButtonDisabled}
          loading={loading}
          onClick={handleOk}
          type="primary"
        >
          {submitText || 'Submit'}
        </UiButton>
        <UiButton danger onClick={handleCancel}>
          {returnText || 'Cancel'}
        </UiButton>
      </UiSpace>
    )
  }

  return (
    <UiModalCore
      {...rest}
      style={style}
      visible={visible}
      title={title}
      onOk={handleOk}
      onCancel={handleCancel}
      footer={renderFooter()}
      width={width}
    >
      {children}
    </UiModalCore>
  )
}

export default UiModal
