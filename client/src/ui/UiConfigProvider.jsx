import React from 'react'
import ConfigProvider from 'antd/lib/config-provider'

const UiConfigProvider = ({ children }) => {
  return <ConfigProvider componentSize="middle">{children}</ConfigProvider>
}

export default UiConfigProvider
