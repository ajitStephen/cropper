import React from 'react'
import Card from 'antd/lib/card'
import 'antd/lib/card/style/css'

const UiCard = ({ title, children, extra, fullscreen, actions, bodyStyle, ...rest }) => (
  <Card
    title={title}
    extra={extra}
    cover={fullscreen && children}
    actions={actions}
    bodyStyle={bodyStyle}
    {...rest}
  >
    {!fullscreen && children}
  </Card>
)

export default UiCard
