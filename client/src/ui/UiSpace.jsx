import React from 'react'
import Space from 'antd/lib/space'
import 'antd/lib/space/style/css'

const UiSpace = ({ children, direction, size = 5, width, align, ...rest }) => (
  <Space wrap size={size} direction={direction} width={width} align={align} {...rest}>
    {children}
  </Space>
)

export default UiSpace
