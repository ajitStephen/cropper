import React from 'react'
import Button from 'antd/lib/button'
import 'antd/lib/button/style/css'

const UiButton = ({
  name,
  disabled,
  style,
  type,
  danger,
  dashed,
  onClick,
  children,
  active,
  size,
  ...rest
}) => (
  <Button
    name={name}
    type={type}
    danger={danger}
    dashed={dashed}
    disabled={disabled}
    onClick={onClick}
    style={style}
    active={active}
    size={size}
    {...rest}
  >
    {children}
  </Button>
)

export default UiButton
