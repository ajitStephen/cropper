import React, { Component } from "react";
import UiResult from "./ui/UiResult";

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
      error: { message: "", stack: "" },
      errorInfo: { componentStack: "" },
    };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { error: true };
  }

  componentDidCatch(error, errorInfo) {
    console.error("componentDidCatch", { error, errorInfo });
    this.setState({ hasError: true, error, errorInfo });
  }

  render() {
    if (this.state.hasError) {
      console.log(this.state);
      return (
        <UiResult
          status={404}
          title={this.state.error?.message}
          message={(this.state.error && this.state.error.stack) || ""}
        />
      );
    }

    return <>{this.props.children}</>;
  }
}
export default ErrorBoundary;
