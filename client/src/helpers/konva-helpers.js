import Colors from '../constants/colors'

export const stroke = {
  color: Colors.YELLOW,
  selectedColor: Colors.BLUE,
  width: 3,
}
