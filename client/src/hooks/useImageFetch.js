import useImage from 'use-image'

const useImageFetch = url => {
  const [image, status] = useImage(url)

  return {
    loading: status === 'loading',
    error: status === 'failed',
    image,
  }
}

export default useImageFetch
