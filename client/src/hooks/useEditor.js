import { useEffect, useRef, useState } from 'react'
import useImageFetch from './useImageFetch'
import UiMessage from '../ui/UiMessage'

// width,height is viewport size
const useEditor = ({ imgData, width, height, setImage, setCropped }) => {
  const [cropping, setCropping] = useState(false)
  const urlSearchParams = new URLSearchParams(window.location.search)
  const { filename } = Object.fromEntries(urlSearchParams.entries())

  const { image, loading, error } = useImageFetch(imgData?.data)
  const aspectRatio = image?.width / image?.height
  let vWidth = image?.width < width ? image?.width : width
  let vHeight = vWidth / aspectRatio
  if (vHeight > height - 60) {
    vHeight = height - 60
    vWidth = vHeight * aspectRatio
  }

  const initialState = {
    x: 20,
    y: 20,
    width: vWidth * 0.7 || 10,
    height: vWidth * 0.7 || 10,
  }

  const trRef = useRef()
  const rectRef = useRef()
  const [cropper, setCropper] = useState(initialState)

  useEffect(() => {
    const timer = setTimeout(() => {
      rectRef?.current?.fire('tap')
    }, [500])

    if (vWidth && vHeight) {
      const shortSide = vWidth < vHeight ? vWidth : vHeight
      setCropper(c => ({
        ...c,
        height: shortSide * 0.7 > vHeight ? vHeight - 20 : shortSide * 0.7,
        width: shortSide * 0.7 > vWidth ? vWidth - 20 : shortSide * 0.7,
      }))
    }

    return () => clearTimeout(timer)
  }, [imgData, vHeight, vWidth])

  const onRectTap = () => {
    trRef?.current?.nodes([rectRef?.current])
    trRef?.current?.getLayer()?.batchDraw()
  }

  const onTransformEnd = e => {
    const node = rectRef.current
    const scaleX = node.scaleX()
    const scaleY = node.scaleY()

    node.scaleX(1)
    node.scaleY(1)
    setCropper(s => ({
      ...s,
      x: node.x(),
      y: node.y(),
      width: Math.max(5, node.width() * scaleX),
      height: Math.max(5, node.height() * scaleY),
    }))
  }

  const onDragEnd = e => {
    if (e) {
      e.cancelBubble = true
    }
    setCropper(s => ({ ...s, x: e?.target?.x(), y: e?.target?.y() }))
  }

  const blobToBase64 = blob => {
    return new Promise((resolve, _) => {
      const reader = new FileReader()
      reader.onloadend = () => resolve(reader.result)
      reader.readAsDataURL(blob)
    })
  }

  const sendFileToXano = async (base64Data, filename) => {
    try {
      // UiMessage.loading({ content: 'Uploading cropped image to xano...', key: 'upload' })
      console.log('request', { base64Data, filename })
      const response = await fetch(process.env.REACT_APP_XANO_END_POINT, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ content: base64Data, filename }),
      })
      const result = await response.json()
      console.log('response', { result })
      // UiMessage.success({ content: 'Uploading Cropped Image Success!', key: 'upload' })
      console.log({ result })
    } catch (err) {
      console.error('send file to xano', err)
      UiMessage.error({ content: 'Uploading to xano failed', key: 'cropping' })
      // throw err
    }
  }

  const saveCrop = async () => {
    try {
      const params = {
        cropper,
        image: { width: image?.width, height: image?.height },
        viewPort: { width: vWidth, height: vHeight },
        imgData,
      }
      console.log('ready to push to server', params)
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(params),
      }

      setCropping(true)
      UiMessage.loading({ content: 'Running cropping...', key: 'cropping', duration: 0 })
      const response = await fetch(
        `${process.env.REACT_APP_CROPPING_SERVER}/crop-image`,
        requestOptions
      )
      const croppedFileBlob = await response.blob()
      const base64Data = await blobToBase64(croppedFileBlob)

      if (process.env.REACT_APP_DOWNLOAD_CROPPED) {
        const url = URL.createObjectURL(croppedFileBlob)
        const a = document.createElement('a')
        a.href = url
        a.download = `cropped-${imgData?.name}`
        a.click()
      }

      if (process.env.REACT_APP_XANO_END_POINT) {
        await sendFileToXano(base64Data, Number(filename))
      }
      UiMessage.success({ content: 'Finished cropping.', key: 'cropping' })
      setImage(null)
      setCropping(false)
      setCropper(initialState)
      setCropped(base64Data)
    } catch (error) {
      console.error('saveCrop', error)
      setCropping(false)
      UiMessage.error({ content: 'Cropping Failed!', key: 'croppinng' })
    }
  }

  return {
    loading,
    error,
    image,
    vWidth,
    vHeight,
    cropper: { ...cropper, rectRef, trRef },
    cropperEventHandlers: { onRectTap, onTransformEnd, onDragEnd },
    saveCrop,
    cropping,
    setCropping,
  }
}

export default useEditor
