import { useState } from 'react'
import useWindowSize from './useWindowSize'

const useMain = () => {
  const window = useWindowSize()
  const [image, setImage] = useState(null)
  const [cropped, setCropped] = useState()

  return { image, ...window, setImage, cropped, setCropped }
}

export default useMain
