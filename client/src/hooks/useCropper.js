const useCropper = ({ width, height, vHeight, vWidth }) => {
  const dragBoundFunc = pos => {
    let X = pos?.x
    let Y = pos?.y
    if (X < 5) {
      X = 5
    }
    if (Y < 5) {
      Y = 5
    }

    if (X + width > vWidth - 5) {
      X = vWidth - width - 5
    }

    if (Y + height > vHeight - 5) {
      Y = vHeight - height - 5
    }
    return { x: X, y: Y }
  }

  const boundBoxFunc = (oldBox, newBox) => {
    // limit resize

    if (newBox.width < 5 || newBox.height < 5) {
      return oldBox
    }

    if (newBox.x < 5) {
      newBox.x = 5
    }

    if (newBox.y < 5) {
      newBox.y = 5
    }

    if (newBox.x + newBox.width > vWidth - 5) {
      newBox.width = vWidth - 5 - newBox.x
    }

    if (newBox.y + newBox.height > vHeight - 5) {
      newBox.height = vHeight - 5 - newBox.y
    }

    if (newBox.height !== oldBox.height) {
      newBox.width = newBox.height
      if (newBox.x + newBox.width > vWidth - 5) {
        newBox.width = vWidth - 5 - newBox.x
      }
    }

    if (newBox.width !== oldBox.width) {
      newBox.height = newBox.width
      if (newBox.y + newBox.height > vHeight - 5) {
        newBox.height = vHeight - 5 - newBox.y
      }
    }

    if (newBox.width !== newBox.height) {
      return oldBox
    }

    return newBox
  }

  return { dragBoundFunc, boundBoxFunc }
}

export default useCropper
