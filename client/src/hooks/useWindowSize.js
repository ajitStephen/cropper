import { useState, useEffect } from 'react'

function getwindowSize() {
  const { innerWidth: width, innerHeight: height } = window
  return {
    width,
    height,
  }
}

export default function useWindowSize() {
  const [windowSize, setwindowSize] = useState(getwindowSize())

  useEffect(() => {
    function handleResize() {
      setwindowSize(getwindowSize())
    }

    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  }, [])

  return windowSize
}
