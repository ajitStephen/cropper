import { useRef } from 'react'

const useUploader = () => {
  const inputRef = useRef()
  const onImageClick = () => {
    inputRef?.current?.click()
  }

  const getBase64 = blobUrl =>
    new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest()
      xhr.responseType = 'blob'
      xhr.onload = () => {
        const recoveredBlob = xhr.response
        const reader = new FileReader()
        reader.onload = function () {
          const blobAsDataUrl = reader.result
          // console.log({ blobAsDataUrl, recoveredBlob })
          resolve(blobAsDataUrl)
        }
        // reader.error = error => reject(error)
        reader.readAsDataURL(recoveredBlob)
      }

      xhr.open('GET', blobUrl)
      xhr.send()
    })

  return { inputRef, onImageClick, getBase64 }
}

export default useUploader
