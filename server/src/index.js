const express = require('express')
const fs = require('fs')
const cors = require('cors')
const bodyParser = require('body-parser')
const { default: cropImage } = require('./cropper')

const app = express()
const port = process.env.PORT || 3001
const whitelist = [
  'http://localhost:3000',
  'http://localhost:3001',
  'https://cropper-tool.netlify.app',
  'http://192.168.0.106:3000',
]

app.use(bodyParser({ limit: '50mb' }))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(
  cors({
    origin: (origin, callback) => {
      console.log({ origin })
      if (!origin || whitelist.indexOf(origin) !== -1) {
        callback(null, true)
      } else {
        callback(new Error())
      }
    },
    methods: ['POST'],
  })
)

app.get('/', (req, res) => {
  res.send('Server Ready!')
})

app.post('/crop-image', async (req, res) => {
  const params = req.body
  const result = await cropImage(params)
  if (!result) {
    throw new Error('Cropping failed')
  }
  const croppedFilePath = `./images/cropped-${params.imgData.name}`
  const originalFilePath = `./images/${params.imgData.name}`

  const stat = fs.statSync(croppedFilePath)

  fs.readFile(croppedFilePath, (error, content) => {
    if (error) {
      res.writeHead(404, { 'Content-Type': 'text/html' })
      res.end('Cropping Failed')
    } else {
      res.writeHead(200, {
        'Content-Type': params.imgData.type,
        'Content-Length': stat?.size,
      })
      res.end(content)
    }

    fs.unlink(croppedFilePath, err => {
      if (err) {
        console.log('Removing cropped file failed...', err)
      } else {
        console.log('Removed Cropped file Success')
      }
    })
    fs.unlink(originalFilePath, err => {
      if (err) {
        console.log('Removing original file failed...', err)
      } else {
        console.log('Removed original file Success')
      }
    })
  })
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
