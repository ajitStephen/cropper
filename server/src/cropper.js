const sharp = require('sharp')
const fs = require('fs')

const readBlobFile = (base64String, name) =>
  // eslint-disable-next-line implicit-arrow-linebreak
  new Promise((resolve, reject) => {
    try {
      const base64Image = base64String.split(';base64,').pop()
      if (!fs.existsSync('./images')) {
        fs.mkdirSync('./images')
      }

      const uploadedFilepath = `./images/${name}`
      fs.writeFile(uploadedFilepath, base64Image, 'base64', err => {
        console.log('File Created')
        if (err) {
          console.log('ERROR', err)
          reject(err)
        }
        const file = fs.readFileSync(uploadedFilepath)
        resolve(file)
      })
    } catch (err) {
      console.log('readBlobFile ERROR', err)
      throw err
    }
  })

const cropImage = async ({ cropper, image, viewPort, imgData }) => {
  // crop at server
  const dx = image.width / viewPort.width
  const dy = image.height / viewPort.height

  const left = Math.round(cropper.x * dx)
  const top = Math.round(cropper.y * dy)
  const width = Math.round(cropper.width * dx)
  const height = Math.round(cropper.height * dy)

  try {
    const file = await readBlobFile(imgData.data, imgData.name)

    await sharp(file)
      .rotate()
      .extract({
        left,
        top,
        width,
        height,
      })
      .withMetadata()
      .toFile(`./images/cropped-${imgData?.name}`)
    return true
  } catch (error) {
    console.log('cropper ERROR', { left, top, width, height }, error)
    throw error
  }
}

export default cropImage
